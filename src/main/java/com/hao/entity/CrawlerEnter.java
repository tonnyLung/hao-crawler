package com.hao.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 爬虫入口
 * </p>
 *
 * @author R.hao
 * @since 2017-07-12
 */
@TableName("crawler_enter")
public class CrawlerEnter implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 入口
     */
    private String enter;

    /**
     * 入口名
     */
    @TableField("enter_name")
    private String enterName;

    /**
     * 状态
     */
    private String status;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;
    /**
     * 分片号
     */
    @TableField("sharding_item")
    private Integer shardingItem;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEnter() {
        return enter;
    }

    public void setEnter(String enter) {
        this.enter = enter;
    }

    public String getEnterName() {
        return enterName;
    }

    public void setEnterName(String enterName) {
        this.enterName = enterName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getShardingItem() {
        return shardingItem;
    }

    public void setShardingItem(Integer shardingItem) {
        this.shardingItem = shardingItem;
    }

}
