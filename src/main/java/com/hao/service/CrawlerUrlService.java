package com.hao.service;

import com.baomidou.mybatisplus.service.IService;
import com.hao.entity.CrawlerEnter;
import com.hao.entity.CrawlerUrl;

import java.util.List;

/**
 * <p>
 * 页面url 服务类
 * </p>
 *
 * @author R.hao
 * @since 2017-07-12
 */
public interface CrawlerUrlService extends IService<CrawlerUrl> {

    /**
     * 运行爬虫
     *
     * @param topLog top日志
     * @param enter  入口
     */
    void runCrawler(String topLog, CrawlerEnter enter);

    /**
     * 锁定url
     *
     * @param list url集合
     * @return 锁定数量
     */
    int clockCrawlerUrl(List<CrawlerUrl> list);

    /**
     * 查询需要爬的Url
     *
     * @param shardingItem 分片号
     * @param totalItem    分片数量
     * @return 集合
     */
    List<CrawlerUrl> findCrawlerUrl(int shardingItem, int totalItem);
}
