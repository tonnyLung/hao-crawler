package com.hao.service;

import com.baomidou.mybatisplus.service.IService;
import com.hao.entity.CrawlerDom;
import com.hao.entity.CrawlerUrl;

/**
 * <p>
 * 页面元素 服务类
 * </p>
 *
 * @author R.hao
 * @since 2017-07-16
 */
public interface CrawlerDomService extends IService<CrawlerDom> {

    /**
     * 执行爬虫
     *
     * @param topLog  top日志
     * @param crawler 爬虫Url
     */
    void runCrawler(String topLog, CrawlerUrl crawler);
}
