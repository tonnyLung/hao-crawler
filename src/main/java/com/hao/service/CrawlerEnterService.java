package com.hao.service;

import com.baomidou.mybatisplus.service.IService;
import com.hao.entity.CrawlerEnter;

/**
 * <p>
 * 爬虫入口 服务类
 * </p>
 *
 * @author R.hao
 * @since 2017-07-12
 */
public interface CrawlerEnterService extends IService<CrawlerEnter> {

    /**
     * 根据分片获取入口
     *
     * @param shardingItem 分片号
     * @return 入口
     */
    CrawlerEnter crawlerEnter(Integer shardingItem);

    /**
     * 锁定入口
     *
     * @param enter 入口
     * @return 数量
     */
    int clockCrawlerEnter(CrawlerEnter enter);

    /**
     * 解锁入口
     *
     * @param enter 入口
     * @return 数量
     */
    int unClockCrawlerEnter(CrawlerEnter enter);

}
