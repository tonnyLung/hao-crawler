package com.hao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hao.entity.CrawlerEnter;
import com.hao.mapper.CrawlerEnterMapper;
import com.hao.service.CrawlerEnterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 爬虫入口 服务实现类
 * </p>
 *
 * @author R.hao
 * @since 2017-07-12
 */
@Service
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class CrawlerEnterServiceImpl extends ServiceImpl<CrawlerEnterMapper, CrawlerEnter> implements CrawlerEnterService {

    @Autowired
    private CrawlerEnterMapper enterMapper;

    @Override
    public CrawlerEnter crawlerEnter(Integer shardingItem) {
        return this.enterMapper.crawlerEnter(shardingItem);
    }

    @Override
    public int clockCrawlerEnter(CrawlerEnter enter) {
        return this.enterMapper.clockCrawlerEnter(enter.getId());
    }

    @Override
    public int unClockCrawlerEnter(CrawlerEnter enter) {
        return this.enterMapper.unClockCrawlerEnter(enter.getId());
    }
}
