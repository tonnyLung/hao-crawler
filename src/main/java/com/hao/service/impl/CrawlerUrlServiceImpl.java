package com.hao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hao.entity.CrawlerEnter;
import com.hao.entity.CrawlerUrl;
import com.hao.mapper.CrawlerUrlMapper;
import com.hao.service.CrawlerEnterService;
import com.hao.service.CrawlerUrlService;
import com.hao.util.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author R.hao
 * @since 2017-07-12
 */
@Service
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class CrawlerUrlServiceImpl extends ServiceImpl<CrawlerUrlMapper, CrawlerUrl> implements CrawlerUrlService {

    private static final Logger logger = LoggerFactory.getLogger(CrawlerUrlServiceImpl.class);

    @Autowired
    private CrawlerUrlMapper urlMapper;

    @Autowired
    private CrawlerEnterService enterService;

    @Override
    public void runCrawler(String topLog, CrawlerEnter enter) {
        try {
            crawlerList(topLog, enter, enter.getEnter());
        } catch (Exception e) {
            logger.error(topLog + "入口异常", e);
            enterService.unClockCrawlerEnter(enter);
        }
    }

    /**
     * 递归解析出需要爬取dom的最终页面url
     *
     * @param topLog top日志
     * @param enter  入口
     * @param url    链接
     * @throws Exception 异常
     */
    private void crawlerList(String topLog, CrawlerEnter enter, String url) throws Exception {
        Document doc = Jsoup.connect(url).get();
        Element div = doc.getElementsByAttributeValue("class", "Clbc_Game_l_a").first();
        Elements as = div.getElementsByAttributeValue("target", "_blank");
        for (Element element : as) {
            if(StringUtils.isEmpty(element.text()))
                continue;
            String absUrl = element.absUrl("href");
            int exist = this.urlMapper.existUrl(absUrl);
            if (exist > 0) {
                continue;
            }
            CrawlerUrl crawlerUrl = new CrawlerUrl();
            crawlerUrl.setCreateTime(new Date());
            crawlerUrl.setEnterId(enter.getId());
            crawlerUrl.setTitle(element.text());
            crawlerUrl.setUrl(absUrl);
            this.urlMapper.insert(crawlerUrl);
        }
        Element nextPage = doc.getElementsMatchingText("下一页").last();
        if (null == nextPage) {
            return;
        }
        String nextUrl = nextPage.absUrl("href");
        //减少循环
        if (nextUrl.contains("10")) {
            return;
        }
        logger.info(topLog + "nextUrl-" + nextUrl);
        crawlerList(topLog, enter, nextUrl);
    }

    @Override
    public int clockCrawlerUrl(List<CrawlerUrl> list) {
        return this.urlMapper.clockCrawlerUrl(list);
    }

    @Override
    public List<CrawlerUrl> findCrawlerUrl(int shardingItem, int totalItem) {
        Map<String, Object> params = new HashMap<>();
        params.put("shardingItem", shardingItem);
        params.put("totalItem", totalItem);
        return this.urlMapper.findCrawlerUrl(params);
    }
}
