package com.hao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * CrawlerApp
 * Created by R.hao on 2017/7/3.
 */
public class CrawlerApp {

    private static final Logger logger = LoggerFactory.getLogger(CrawlerApp.class);

    public static void main(String[] args) {
        logger.info("========== hao-crawler start ==========");
        new ClassPathXmlApplicationContext("classpath:applicationContext.xml",
                "classpath:elastic-job-lite.xml");
    }

}
