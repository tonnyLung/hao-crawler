package com.hao.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hao.entity.CrawlerEnter;

/**
 * <p>
 * 爬虫入口 Mapper 接口
 * </p>
 *
 * @author R.hao
 * @since 2017-07-12
 */
public interface CrawlerEnterMapper extends BaseMapper<CrawlerEnter> {

    /**
     * 根据分片获取入口
     *
     * @param shardingItem 分片号
     * @return 入口
     */
    CrawlerEnter crawlerEnter(Integer shardingItem);

    /**
     * 锁定入口
     *
     * @param id 入口id
     * @return 数量
     */
    int clockCrawlerEnter(Integer id);

    /**
     * 解锁入口
     *
     * @param id 入口id
     * @return 数量
     */
    int unClockCrawlerEnter(Integer id);

}