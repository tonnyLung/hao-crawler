package com.hao.mapper;

import com.hao.entity.CrawlerDom;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 页面元素 Mapper 接口
 * </p>
 *
 * @author R.hao
 * @since 2017-07-16
 */
public interface CrawlerDomMapper extends BaseMapper<CrawlerDom> {

}