package com.hao.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hao.entity.CrawlerUrl;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author R.hao
 * @since 2017-07-12
 */
public interface CrawlerUrlMapper extends BaseMapper<CrawlerUrl> {

    /**
     * 存在url
     *
     * @param url url
     * @return 结果
     */
    int existUrl(String url);

    /**
     * 寻找负责的url
     *
     * @param params 参数
     * @return
     */
    List<CrawlerUrl> findCrawlerUrl(Map<String, Object> params);

    /**
     * 锁定url
     *
     * @param list 集合
     * @return 数量
     */
    int clockCrawlerUrl(List<CrawlerUrl> list);
}