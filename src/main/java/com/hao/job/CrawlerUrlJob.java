package com.hao.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.hao.entity.CrawlerEnter;
import com.hao.service.CrawlerEnterService;
import com.hao.service.CrawlerUrlService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 爬虫url任务
 * Created by R.hao on 2017/7/12.
 */
public class CrawlerUrlJob implements SimpleJob {

    private static final Logger logger = LoggerFactory.getLogger(CrawlerUrlJob.class);

    @Autowired
    private CrawlerEnterService enterService;

    @Autowired
    private CrawlerUrlService urlService;

    @Override
    public void execute(ShardingContext context) {
        String topLog = "CrawlerUrlJob-shardingItem-" + context.getShardingItem() + "-"
                + "totalItem-" + context.getShardingTotalCount() + "-";
        logger.info(topLog);
        CrawlerEnter enter = this.enterService.crawlerEnter(context.getShardingItem());
        if (null == enter) {
            logger.info(topLog + "not find enter");
            return;
        }
        this.enterService.clockCrawlerEnter(enter);
        this.urlService.runCrawler(topLog, enter);
        this.enterService.unClockCrawlerEnter(enter);
    }
}
