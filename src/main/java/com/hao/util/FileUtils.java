package com.hao.util;

import java.io.File;

/**
 * 文件工作类
 * Created by R.hao on 2017/7/16.
 */
public class FileUtils {

    public static String fileName(String url) {
        if (!url.contains(".")) {
            return null;
        }
        if (!url.contains("/")) {
            return url;
        }
        String[] paths = url.split("/");
        if (paths.length == 0) {
            return null;
        }
        return paths[paths.length - 1];
    }

    public static boolean isEmptyFolder(File folder) {
        if (null == folder)
            return true;
        if (null == folder.list())
            return true;
        if (folder.list().length == 0)
            return true;
        return false;
    }
}
