package com.hao.util;

import java.util.UUID;

/**
 * UUID 生成
 * Created by R.hao on 2017/5/11.
 */
public class GenerateUUID {

    public static String uuid36() {
        return UUID.randomUUID().toString();
    }

    public static String uuid32() {
        return uuid36().replace("-", "");
    }
}
