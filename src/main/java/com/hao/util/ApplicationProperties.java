package com.hao.util;

import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * 读取系统配置文件
 * Created by R.hao on 2016/5/3 19:20.
 */
public class ApplicationProperties {

    private final static org.slf4j.Logger logger = LoggerFactory.getLogger(ApplicationProperties.class);

    private static Properties properties = new Properties();


    public static String FILE_SAVE_PATH;

    private static String getValue(String key) {
        if (StringUtils.isEmpty(key)) return null;
        try {
            return properties.getProperty(key);
        } catch (Exception e) {
            logger.error("application.properties文件中不包含KEY=" + key + "的数据", e);
        }
        return null;
    }

    static {
        InputStream is = ApplicationProperties.class.getResourceAsStream("/application.properties");
        try {
            properties.load(new InputStreamReader(is, "UTF-8"));
            FILE_SAVE_PATH = getValue("file.savePath");
        } catch (Exception e) {
            logger.error("不能读取属性文件.请确保application.properties在CLASSPATH指定的路径中", e);
        }
    }

}
