package com.hao.util.date;

import org.apache.log4j.Logger;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 日期工具类
 * Created by R.hao on 2017/5/23.
 */
public class DateUtil {

    private static final Logger logger = Logger.getLogger(DateUtil.class);

    public static String toString(Date date, DatePattern patten) {
        DateFormat dateFormat = new SimpleDateFormat(patten.getPattern());
        return dateFormat.format(date);
    }

    public static String toString(Date date) {
        return toString(date, DatePattern.DEFAULT);
    }

    public static Date parse(String source) {
        return parse(source, DatePattern.DEFAULT);
    }

    public static Date parse(String source, DatePattern pattern) {
        DateFormat dateFormat = new SimpleDateFormat(pattern.getPattern());
        try {
            return dateFormat.parse(source);
        } catch (ParseException e) {
            logger.error("to date is falied", e);
        }
        return null;
    }
}
