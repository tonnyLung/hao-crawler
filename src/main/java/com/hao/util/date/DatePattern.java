package com.hao.util.date;

/**
 * Date规则
 * Created by R.hao on 2017/1/24.
 */
public enum DatePattern {

    DEFAULT("yyyy-MM-dd HH:mm:ss"),
    DAY("yyyy-MM-dd");

    private String pattern;

    DatePattern(String pattern) {
        this.pattern = pattern;
    }

    public String getPattern() {
        return pattern;
    }
}
