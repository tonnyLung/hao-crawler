package com.hao.util.json;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;

import java.util.Collection;

/**
 * JsonUtil
 * Created by R.hao on 2017/1/12.
 */
public class JsonUtil {

    public static String toString(Object o) {
        JsonValueFilter valueFilter = new JsonValueFilter();
        if (o instanceof Collection) {
            return JSONArray.toJSONString(o, valueFilter,SerializerFeature.WriteMapNullValue);
        } else if (o instanceof String) {
            return (String) o;
        }
        return JSONObject.toJSONString(o, valueFilter,SerializerFeature.WriteMapNullValue);
    }

    public static JSONObject parseObject(Object o) {
        return JSONObject.parseObject(toString(o));
    }

    public static JSONArray parseArray(Object o) {
        return JSONArray.parseArray(toString(o));
    }

    public static <T> T parseObject(String s, Class<T> cls) {
        return JSONObject.parseObject(s, cls);
    }

}
