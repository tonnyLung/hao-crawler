package com.hao.util.json;

import com.alibaba.fastjson.serializer.ValueFilter;
import com.hao.util.date.DateUtil;

import java.util.Date;

public class JsonValueFilter implements ValueFilter {
    /**
     * @param object 对象
     * @param name   对象的字段的名称
     * @param value  对象的字段的值
     */
    @Override
    public Object process(Object object, String name, Object value) {
        if (null != value && value instanceof Date) {
            Date ret = (Date) value;
            return DateUtil.toString(ret);
        }
        return value;
    }
}  