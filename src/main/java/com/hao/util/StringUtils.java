package com.hao.util;


import java.util.Collection;
import java.util.UUID;

/**
 * 字符串的一些常用操作
 *
 * @author R.hao
 */
public class StringUtils extends org.springframework.util.StringUtils {

    public static boolean isBlank(final CharSequence cs) {
        return !StringUtils.isNotBlank(cs);
    }

    public static boolean isNotBlank(final CharSequence cs) {
        return StringUtils.hasText(cs);
    }

    public static String join(Collection<?> coll, String delim) {
        return StringUtils.collectionToDelimitedString(coll, delim);
    }

    public static String join(Object[] arr, String delim) {
        return StringUtils.arrayToDelimitedString(arr, delim);
    }

    public static String getUUId() {
        return UUID.randomUUID().toString();
    }

    public static String hidenMobile(String mobile) {
        if (isEmpty(mobile))
            return mobile;
        return mobile.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2");
    }

    public static String join(String split, String... ss) {
        String result = "";
        for (String s : ss) {
            result += split + s;
        }
        if (isEmpty(result))
            return null;
        return result.substring(1);
    }

    public static String trim(String s) {
        if (isEmpty(s))
            return s;
        return s.trim();
    }
}
