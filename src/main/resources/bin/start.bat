@echo off

cd ..
set JAVA_OPTS= -Dfile.encoding=utf-8 -Djava.awt.headless=true -Djava.net.preferIPv4Stack=true -Djava.library.path=%JAVA_HOME%\lib

setlocal enabledelayedexpansion
for %%j in (%cd%\lib\*.jar) do (
	set LIB_DIR=!LIB_DIR!%%j;
)

java %JAVA_OPT% -classpath %LIB_DIR%:. com.hao.CrawlerApp

pause