#!/bin/bash
cd `dirname $0`
cd ..
DEPLOY_DIR=`pwd`
LIB_DIR=${DEPLOY_DIR}/lib/*
JAVA_OPTS=" -Dfile.encoding=utf-8 -Djava.awt.headless=true -Djava.net.preferIPv4Stack=true -Djava.library.path=/usr/local/lib:/usr/lib:/usr/lib64:/usr/local/lib"
nohup java ${JAVA_OPT} -classpath ${LIB_DIR}:. com.hao.CrawlerApp >/dev/null 2>&1 &
