package com.hao;

import com.hao.entity.CrawlerUrl;
import com.hao.service.CrawlerDomService;
import com.hao.service.CrawlerUrlService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * CrawlerDom测试
 * Created by R.hao on 2017/7/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext.xml")
public class CrawlerDomTest {

    private static final Logger logger = LoggerFactory.getLogger(CrawlerDomTest.class);

    @Autowired
    private CrawlerDomService domService;

    @Autowired
    private CrawlerUrlService urlService;

    @Test
    public void runCrawler() {
        int shardingItem = 0;
        int totalItem = 3;

        String topLog = "爬虫dom任务，shardingItem-" + shardingItem + "-"
                + "totalItem-" + totalItem + "-";
        List<CrawlerUrl> urlList = this.urlService.findCrawlerUrl(shardingItem, totalItem);
        if (null == urlList || urlList.isEmpty()) {
            logger.info(topLog + "未获取到数据");
            return;
        }
        for (CrawlerUrl crawlerUrl : urlList) {
            this.domService.runCrawler(topLog, crawlerUrl);
        }
    }
}
