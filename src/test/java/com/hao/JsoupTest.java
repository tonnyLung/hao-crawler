package com.hao;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.Test;

import java.io.IOException;

/**
 * jsoup测试
 * Created by R.hao on 2017/7/12.
 */
public class JsoupTest {

    @Test
    public void run() {
        try {
            Document doc = Jsoup.connect("http://699pic.com/tupian/shatan.html").get();
            Elements elements = doc.getElementsByAttributeValue("class","lazy");
            System.out.println(elements.hasAttr("height"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
