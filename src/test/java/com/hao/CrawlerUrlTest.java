package com.hao;

import com.hao.entity.CrawlerEnter;
import com.hao.service.CrawlerEnterService;
import com.hao.service.CrawlerUrlService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * CrawlerUrl测试
 * Created by R.hao on 2017/7/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext.xml")
public class CrawlerUrlTest {

    private static final Logger logger = LoggerFactory.getLogger(CrawlerUrlTest.class);

    @Autowired
    private CrawlerEnterService enterService;

    @Autowired
    private CrawlerUrlService urlService;

    @Test
    public void runCrawler() {
        int shardingItem = 0;
        String topLog = "分片号-" + shardingItem;

        CrawlerEnter enter = this.enterService.crawlerEnter(shardingItem);
        if (null == enter) {
            logger.info(topLog + "未找到入口");
            return;
        }
        //执行
        this.urlService.runCrawler(topLog, enter);
    }

    @Test
    public void clockCrawlerEnter() {
        CrawlerEnter enter = this.enterService.crawlerEnter(0);
        this.enterService.unClockCrawlerEnter(enter);
    }
}
