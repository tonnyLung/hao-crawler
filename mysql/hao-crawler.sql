/*
Navicat MySQL Data Transfer

Source Server         : 118.89.177.43
Source Server Version : 50718
Source Host           : 118.89.177.43:3306
Source Database       : hao-crawler

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2017-07-31 15:34:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for crawler_dom
-- ----------------------------
DROP TABLE IF EXISTS `crawler_dom`;
CREATE TABLE `crawler_dom` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `type` varchar(255) NOT NULL COMMENT '类型 0图片 1文本 2其他',
  `content` varchar(255) DEFAULT NULL COMMENT '内容',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `url_id` int(11) DEFAULT NULL COMMENT '父页面ID',
  `status` int(2) DEFAULT '0' COMMENT '状态 0未完成 1完成 2锁定 3下载失败',
  PRIMARY KEY (`id`),
  KEY `idx_page_dom_title` (`title`),
  KEY `idx_page_dom_type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='页面元素';

-- ----------------------------
-- Records of crawler_dom
-- ----------------------------

-- ----------------------------
-- Table structure for crawler_enter
-- ----------------------------
DROP TABLE IF EXISTS `crawler_enter`;
CREATE TABLE `crawler_enter` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `enter` varchar(255) NOT NULL COMMENT '入口',
  `enter_name` varchar(255) NOT NULL COMMENT '入口名',
  `status` varchar(2) NOT NULL COMMENT '状态 0待执行 1完成 2锁定 3失败',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `sharding_item` int(2) NOT NULL COMMENT '分片号',
  PRIMARY KEY (`id`),
  KEY `idx_crawler_enter` (`enter`) USING BTREE,
  KEY `uk_crawler_enter_sharding_item` (`sharding_item`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='爬虫入口';

-- ----------------------------
-- Records of crawler_enter
-- ----------------------------
INSERT INTO `crawler_enter` VALUES ('1', 'http://www.mmonly.cc/ktmh', 'mmonly', '0', '2017-07-31 14:23:53', '0');

-- ----------------------------
-- Table structure for crawler_url
-- ----------------------------
DROP TABLE IF EXISTS `crawler_url`;
CREATE TABLE `crawler_url` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(1024) NOT NULL COMMENT '标题',
  `url` varchar(1024) NOT NULL COMMENT '地址',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `parent_id` int(11) DEFAULT NULL COMMENT '父节点ID',
  `enter_id` int(11) DEFAULT NULL COMMENT '入口id',
  `status` int(2) DEFAULT '0' COMMENT '状态 0未完成 1完成 2锁定',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='页面url ';

-- ----------------------------
-- Records of crawler_url
-- ----------------------------
