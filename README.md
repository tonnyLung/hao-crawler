#hao分布式爬虫系统


##项目架构
- 分布式管理：zookeeper 3.4.9
- 任务管理：elastic-job-lite 2.1.4
- 核心框架：spring 4
- 持久层框架：mybatis-plus 2.0.8
- html解析器：jsoup 1.10.3

## 使用说明
- 修改urlService和domService的runCrawler方法实现
- 将urlService和domService编写自己成的html解析
- 其他自行扩展

## 本地运行
- 本地开发需要安装zookeeper或有zookeeper服务器
- 运行CrawlerApp即可

##服务器部署
- 执行mvn assembly:assembly -Dmaven.test.skip=true 命令将工程打包
- 打包后会出现一个jar包和一个tar.gz包，如果是第一次部署需要把tar.gz包放到服务器，二次部署只需要把jar包放到lib目录下即可

## 服务器运行
- 进入bin目录
- ./start.sh

## QQ交流群
- 335320920

## 更新说明
###V1.1
- enter表：去除index_url字段，添加enter_name字段
- 添加样例数据
- 样例代码编写
